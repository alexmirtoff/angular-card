import { Component, OnInit } from '@angular/core';
import { UsersService } from '../users.service'

@Component({
  selector: 'app-setup-page',
  templateUrl: './setup-page.component.html',
  styleUrls: ['./setup-page.component.css']
})
export class SetupPageComponent implements OnInit {

  sizze;

  constructor(private usersService: UsersService) { }


  ngOnInit() {
    this.sizze = this.usersService.size;
  }
  
  onChange() {
    this.usersService.setSize(+this.sizze)
  }

}
